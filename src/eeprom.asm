;   EEPROM - Save a byte using PIC's EEPROM
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of EEPROM
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;EEPROM (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the counter
; - RB1 - 7 - bit 1 of the counter
; - RB2 - 8 - bit 2 of the counter
; - RB3 - 9 - bit 3 of the counter
; - RB4 - 10 - bit 4 of the counter
; - RB5 - 11 - bit 5 of the counter
; - RB6 - 12 - bit 6 of the counter
; - RB7 - 13 - bit 7 of the counter
; - RA1 - 18 - Input switch for counter change
; - RA2 - 1 - Save to EEPROM switch
; - RA3 - 2 - Show if data is saved to EEPROM
;=============================

;Variables
;==============================
COUNTER EQU 0x27
BTNPRESSED EQU 0x28

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start		
	
;Functions
;==============================
	include digitecnology.inc
ChangeCounter
	call chgbnk0
	bcf PORTA,3
	incf COUNTER,1
	return
SaveCounter
	call chgbnk1
	movlw 0x00 ;Address 0x00 of EEPROM
	movwf EEADR
	call chgbnk0
	movf COUNTER,0
	call chgbnk1
	movwf EEDATA
	bsf EECON1,2 ;Enable write
	;Do special thing for write to EEPROM
	movlw 0x55
	movwf EECON2
	movlw 0xAA
	movwf EECON2
	bsf EECON1,1 ;Begin Write
	btfsc EECON1,1 ;Wait for complete
	goto $-1
	bcf EECON1,2 ;Disable write
	btfsc EECON1,3 ;Check for errors
	goto $+3
	call chgbnk0
	bsf PORTA,3 ;Turn the LED for complete write	
	return
LoadCounter
	call chgbnk1
	movlw 0x00 ;Address 0x00 of EEPROM
	movwf EEADR
	bsf EECON1,0 ;Read from EEPROM
	btfsc EECON1,0 ;Wait for complete
	goto $-1
	movf EEDATA,0 ;Move data to PORTB
	call chgbnk0
	movwf COUNTER
	return
ShowCounter
	call chgbnk0
	movf COUNTER,0
	movwf PORTB
	return
Wait	
	;Wait 10 ms
	movlw .246
	movwf DELAY1
	movlw .14
	movwf DELAY2
	call delay2
	return
;Program
;==============================
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	clrf TRISB ;Outputs
	bsf TRISA,1 ;Input
	bsf TRISA,2 ;Input
	bcf TRISA,3 ;Output
	;Deactivate analog comparators
	call chgbnk0
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0	
	;Clear counter
	clrf COUNTER	
	;Load data from EEPROM
	call LoadCounter	
	call ShowCounter
Cycle			
	;Check input (counter change)
	btfsc PORTA,1
	goto $+0xA ;10
	call Wait
	btfsc PORTA,1
	goto $+7
	btfsc BTNPRESSED,0
	goto $+6
	call ChangeCounter
	call ShowCounter
	bsf BTNPRESSED,0
	goto $+2
	bcf BTNPRESSED,0
	;Check input (EEPROM write)
        btfsc PORTA,2
        goto $+9
        call Wait
        btfsc PORTA,2
        goto $+6
        btfsc BTNPRESSED,1
        goto $+5
        call SaveCounter        
        bsf BTNPRESSED,1
        goto $+2
        bcf BTNPRESSED,1
	goto Cycle
	end

